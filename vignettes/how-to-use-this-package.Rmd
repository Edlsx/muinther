---
title: "how-to-use-this-package"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{how-to-use-this-package}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---


```{r setup}
library(muinther)
```
#Pearson contingence table
```{r}
pearsontable(docs_phenotype_file_1)
```
# Entropy outputs computation on a csv file
```{r}
loop('I:/muinther/R/muinther/docs_phenotype_file_1.csv',1,8)
```
#Heatmap output
```{r}
heatmap2(entropy_outputs)
```
